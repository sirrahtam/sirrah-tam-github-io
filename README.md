# sirrahtam

[![Netlify Status](https://api.netlify.com/api/v1/badges/7ed8b366-9875-4adb-ae0d-27c391e88a51/deploy-status)](https://app.netlify.com/sites/hopeful-pasteur-41e044/deploys)

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Run your tests
```
yarn test
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
